var Docentes=(function (){

  var docentes=[];
  
  
  function listarDocentes(){
    return docentes;
  }
  
  function agregarDocente(objeto){
    
     var contenedor=existeDocente(objeto);
    
    if(contenedor.existe)
      console.log("Docente ya registrado,escoga otro ID");
      else{
       
        var id=objeto.id.toString();
        var rfc=objeto.rfc;
        var nombre=objeto.nombre;
        var apellidoPaterno=objeto.apellidoPaterno;
        var apellidoMaterno=objeto.apellidoMaterno;
        var grupos=[];
        var materias=objeto.materias;
        
        
        if( id.length>1 && (rfc!==undefined && rfc.length>=12 && rfc.length<=13) && (nombre!==undefined && nombre.length>1) && (apellidoPaterno!==undefined && apellidoPaterno.length>1) && (apellidoPaterno!==undefined && apellidoMaterno.length>1) && grupos!==undefined  && materias!==undefined){
            docentes.push(objeto);
            console.log("Se agrego correctamente al Docente");
        }
        else
          console.log("Numero de ID invalido, o faltan  atributos");
    }
    
  
  }
  
  function asignarGrupos(objeto){
    
    for(var i in docentes){
      if(docentes[i][objeto.atributo]===objeto.igual_a){
        docentes[i].grupos.push(objeto.grupo);
      }
    }
  
  }
  
  
  
  
  function existeDocente(objeto){
    var indice=0;
    for(var i in docentes){ 
        if(docentes[i].id===objeto.id){
           indice=i;
           return {
            "existe":true,
            "indice":indice
           
           };
         }
    }
    return {
      "existe":false,
      "indice":indice
           
    } ;
  }
  
  
  function buscar(objeto){
    var arrayTemp=[];
    for(var i in docentes){
      if(docentes[i][objeto.atributo]===objeto.igual_a){
      arrayTemp.push(docentes[i]);
      }
    }
    return arrayTemp;
  }
  
  
  function buscarEspecifico(otraFuncion){
    var arrayAux=[];
    for(var i in docentes){
      if(   otraFuncion(docentes[i])   ){
        arrayAux.push(docentes[i]);
      }
    }
    return arrayAux;
  }
  
  
  function eliminarDocente(objeto){
    
    var contenedor=existeDocente(objeto);
    
    if(contenedor.existe){
      docentes.splice(contenedor.indice,1);
      console.log("Docente Eliminado Correctamente");
    }
    else
      console.log("No existe el docente");
    
  }
  
  
  
   function modificarDocente(objeto){
      
      var indice=0;
      var contenedor=existeDocente(objeto);
      if(contenedor.existe){
        
          indice=contenedor.indice;
          docentes[indice].id=objeto.id;
          docentes[indice].rfc=objeto.rfc;
          docentes[indice].nombre=objeto.nombre;
          docentes[indice].apellidoPaterno=objeto.apellidoPaterno;
          docentes[indice].apellidoMaterno=objeto.apellidoMaterno;
          docentes[indice].grupos=objeto.grupos;
          docentes[indice].materias=objeto.materias;
          console.log("Docente Modificado Correctamente");
                                 
        }
        else
          console.log("No existe el docente");
    
  }
  
  
  
  
  
  return {
    "agregarDocente":agregarDocente,
    "listarDocentes":listarDocentes,
    "buscar":buscar,
    "buscarEspecifico":buscarEspecifico,
    "eliminarDocente":eliminarDocente,
    "modificarDocente":modificarDocente,
    "existeDocente":existeDocente,
    "docentes":docentes,
    "asignarGrupos":asignarGrupos
    
    
   
  };


})();
