var Grupos=(function (){

  var grupos=[];
  
  
  
  function listarGrupos(){
    return grupos;
  }
  
  function agregarGrupo(objeto){
    
     var contenedor=existeGrupo(objeto);
    
    if(contenedor.existe)
      console.log("Grupo ya registrado,escoga otro ID");
      else{
        var id=objeto.id.toString();
        var clave=objeto.clave;
        var nombre=objeto.nombre;
        var alumnos=objeto.alumnos;
        var docentes=[];
        var materias=objeto.materias;

        
        
        if((id.length>0 && id!==undefined) && (clave.length>0 && clave!==undefined) && (nombre.length>0 && nombre!==undefined) && alumnos!==undefined && docentes!==undefined && materias!==undefined){
            grupos.push(objeto);
            console.log("Se agrego correctamente el grupo");
        }
        else
          console.log("Numero de ID invalido, o faltan  atributos");
    }
    
  
  }
  
  
  function existeGrupo(objeto){
    var indice=0;
    for(var i in grupos){ 
        if(grupos[i].id===objeto.id){
           indice=i;
           return {
            "existe":true,
            "indice":indice
           
           };
         }
    }
    return {
      "existe":false,
      "indice":indice
           
    } ;
  }
  
  
  function buscar(objeto){
    var arrayTemp=[];
    for(var i in grupos){
      if(grupos[i][objeto.atributo]===objeto.igual_a){
      arrayTemp.push(grupos[i]);
      }
    }
    return arrayTemp;
  }
  
  
  function buscarEspecifico(otraFuncion){
    var arrayAux=[];
    for(var i in grupos){
      if(   otraFuncion(grupos[i])   ){
        arrayAux.push(grupos[i]);
      }
    }
    return arrayAux;
  }
  
  
  function eliminarGrupo(objeto){
    
    var contenedor=existeGrupo(objeto);
    
    if(contenedor.existe){
      grupos.splice(contenedor.indice,1);
      console.log("Grupo Eliminado Correctamente");
    }
    else
      console.log("No existe el grupo");
    
  }
  
  
  
   function modificarGrupo(objeto){
      
      var indice=0;
      var contenedor=existeGrupo(objeto);
      if(contenedor.existe){
        
          indice=contenedor.indice;
          grupos[indice].id=objeto.id;
          grupos[indice].clave=objeto.clave;
          grupos[indice].nombre=objeto.nombre;
          grupos[indice].alumnos=objeto.alumnos;
          grupos[indice].docentes=objeto.docentes;
          grupos[indice].materias=objeto.materias;
          console.log("Grupo Modificado Correctamente");
                                 
        }
        else
          console.log("No existe el Grupo");
    
  }
  
  
  function asignarDocentes(objeto){
    for(var i in grupos){
      if(grupos[i][objeto.atributo]==objeto.igual_a){
        grupos[i].docentes.push(objeto.docente);
      }
    }
  }
  
  
  function agregarDocente(objeto){
     
     
     var existeG=existeGrupo(objeto);
      
      
     var existeDocente=Docentes.existeDocente( {'id':objeto.docente} );
     
     if(existeG.existe){
      if(existeDocente.existe){
        Docentes.asignarGrupos({ 'atributo':'id','igual_a':objeto.docente,'grupo':grupos[existeG.indice]});
        asignarDocentes({'atributo':'id','igual_a':objeto.id,'docente':Docentes.buscar({'atributo':'id','igual_a':objeto.docente})});
      }
      else
        console.log("No existe el docente");
        
     }
     else
      console.log("NO existe el grupo");
     
     
    
  }
  
  
  
  
  return {
    "agregarGrupo":agregarGrupo,
    "listarGrupos":listarGrupos,
    "buscar":buscar,
    "buscarEspecifico":buscarEspecifico,
    "eliminarGrupo":eliminarGrupo,
    "modificarGrupo":modificarGrupo,
    "agregarDocente":agregarDocente,
    "existeGrupo":existeGrupo
  };


})();
