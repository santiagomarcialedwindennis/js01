var Alumnos=(function (){
  
  var alumnos=[];
  
  function _listarAlumnos(){
    return alumnos;
  }
  
  
  function _agregarAlumno(objeto){
    
    var contenedor=existeAlumno(objeto);
    
    if(contenedor.existe)
      console.log("Alumno ya registrado,escoga otro numero de control");
      else{
        var numero_de_control=objeto.numero_de_control.toString();
        var nombre=objeto.nombre;
        var apellidoPaterno=objeto.apellidoPaterno;
        var apellidoMaterno=objeto.apellidoMaterno;
        var genero=objeto.genero;
        var calificaciones=objeto.calificaciones;
        
        
        if( numero_de_control.length==8 && (nombre!==undefined && nombre.length>1) && (apellidoPaterno!==undefined && apellidoPaterno.length>1) && (apellidoPaterno!==undefined && apellidoMaterno.length>1) && (genero!==undefined && genero.length==1) && calificaciones!==undefined){
            alumnos.push(objeto);
            console.log("Se agrego correctamente al alumno");
        }
        else
          console.log("Numero de control invalido, o faltan  atributos");
    }
  }
  
  
  function _buscar(objeto){
    var arrayTemp=[];
    for(var i in alumnos){
      if(alumnos[i][objeto.atributo]===objeto.igual_a){
      arrayTemp.push(alumnos[i]);
      }
    }
    return arrayTemp;
  }
  
  
  function buscarEspecifico(otraFuncion){
    var arrayAux=[];
    for(var i in alumnos){
      if(   otraFuncion(alumnos[i])   ){
        arrayAux.push(alumnos[i]);
      }
    }
    return arrayAux;
  }
  
  
  
  function eliminarAlumno(objeto){
    
    var contenedor=existeAlumno(objeto);
    
    if(contenedor.existe){
      alumnos.splice(contenedor.indice,1);
      console.log("Alumno Eliminado Correctamente");
    }
    else
      console.log("No existe el alumno");
    
  }
  
  
  function existeAlumno(objeto){
    var indice=0;
    for(var i in alumnos){ 
        if(alumnos[i].numero_de_control===objeto.numero_de_control){
           indice=i;
           return {
            "existe":true,
            "indice":indice
           
           };
         }
    }
    return {
      "existe":false,
      "indice":indice
           
    } ;
  }
  
  
  function modificarAlumno(objeto){
      
      var indice=0;
      var contenedor=existeAlumno(objeto);
      if(contenedor.existe){
        
          indice=contenedor.indice;
          alumnos[indice].numero_de_control=objeto.numero_de_control;
          alumnos[indice].nombre=objeto.nombre;
          alumnos[indice].apellidoPaterno=objeto.apellidoPaterno;
          alumnos[indice].apellidoMaterno=objeto.apellidoMaterno;
          alumnos[indice].genero=objeto.genero;
          alumnos[indice].calificaciones=objeto.calificaciones;
          console.log("Alumno Modificado Correctamente");
                                 
        }
        else
          console.log("No existe el alumno");
    
  }
  
  
  
  
  
  
  
  
  return {
  
    "listarAlumnos":_listarAlumnos,
    "agregarAlumno":_agregarAlumno,
    "buscar":_buscar,
    "buscarEspecifico":buscarEspecifico,
    "modificarAlumno":modificarAlumno,
    "eliminarAlumno":eliminarAlumno
    
  
  };

})();
