Alumnos.agregarAlumno(
  {
    'numero_de_control':10160903,
    'nombre':'Edwin Dennis',
    'apellidoPaterno':'santiago',
    'apellidoMaterno':'marcial',
    'genero':'M',
    'calificaciones':100
  }
  );
  
Alumnos.agregarAlumno(
  {
    'numero_de_control':12160945,
    'nombre':'cristian ivan',
    'apellidoPaterno':'perez',
    'apellidoMaterno':'perez',
    'genero':'M',
    'calificaciones':90
  }
  );
Alumnos.agregarAlumno(
  {
    'numero_de_control':10160902,
    'nombre':'Ricardo',
    'apellidoPaterno':'martinez',
    'apellidoMaterno':'lopez',
    'genero':'M',
    'calificaciones':80
  }
  );
  
  Alumnos.listarAlumnos();
  
  Alumnos.buscar({'atributo':'numero_de_control','igual_a':10160903});
  Alumnos.buscarEspecifico(
    function(alumno){
      if(alumno.numero_de_control===10160903 && alumno.genero==='M')
        return true;
      else
        return false;
    }
    
);

Alumnos.modificarAlumno({
  
  'numero_de_control':10160903,
  'nombre':'edwin dennis',
  'apellidoPaterno':'Gonzales', //<--- cambiamos de apellido
  'apellidoMaterno':'marcial',
  'genero':'M',
  'calificaciones':100
  
});

Alumnos.listarAlumnos();

Alumnos.eliminarAlumno({
  "numero_de_control":10160902   //  <--valido
});

Alumnos.listarAlumnos();

Alumnos.eliminarAlumno({
  "numero_de_control":10000000   //  <--no valido no existe
});

Alumnos.listarAlumnos();


Docentes.agregarDocente({
  
  'id':12,
  'rfc':'ASDFAS352625',
  'nombre':'pablo',
  'apellidoPaterno':'juarez',
  'apellidoMaterno':'jimenez',
  'grupos':'sin asignar',
  'materias':'sin asignar'
  
});

Docentes.listarDocentes();

  Docentes.buscar({'atributo':'id','igual_a':12});
 
  Docentes.buscarEspecifico(
    function(docente){
      if(docente.id===12 && docente.rfc==='ASDFAS352625')
        return true;
      else
        return false;
    }
    
);

Docentes.modificarDocente({
  
  'id':12,
  'rfc':'UNODOSTRESCUA',  //<--- cambiamos de rfc
  'nombre':'pablo',
  'apellidoPaterno':'juarez', 
  'apellidoMaterno':'jimenez',
  'grupos':'sin definir',
  'materias':'sin definir'
  
});

Docentes.listarDocentes();

Docentes.eliminarDocente({
  "id":12   //  <--valido
});

Docentes.listarDocentes();

Docentes.eliminarDocente({
  "id":1000231124   //  <--no valido no existe
});

  
//------->



Grupos.agregarGrupo({
  
  'id':12126,
  'clave':'20DPR3047Z',
  'nombre':'Herramientas',
  'alumnos':'sin asignar',
  'docentes':'sin asignar',
  'materias':'sin asignar'
  
});

Grupos.listarGrupos();

Grupos.buscar({'atributo':'nombre','igual_a':'Herramientas'});
 
Grupos.buscarEspecifico(
    function(grupo){
      if(grupo.id===12126 && grupo.nombre==='Herramientas')
        return true;
      else
        return false;
    }
    
);

Grupos.modificarGrupo({
  
  'id':12126,
  'clave':'UNODOSTRESCUA',  //<--- cambiamos la clave
  'nombre':'pablo',
  'alumnos':'sin definir', 
  'docentes':'sin definir',
  'materias':'sin definir'
  
});

Grupos.listarGrupos();

Grupos.eliminarGrupo({
  "id":12126   //  <--valido
});

Grupos.listarGrupos();

Grupos.eliminarGrupo({
  "id":1000231124   //  <--no valido no existe
});


//--------------------->

var alumnos=Alumnos();
  var docentes=Docentes();
  var grupos=Grupos();

 
Docentes.agregarDocente({
  
  'id':8272,
  'rfc':'unoDoscincoNu',
  'nombre':'Juan',
  'apellidoPaterno':'manuel',
  'apellidoMaterno':'sanchez',
  'grupos':'sin asignar',
  'materias':'sin asignar'
  
});



 Docentes.agregarDocente({
  
  'id':12,
  'rfc':'ASDFAS352625',
  'nombre':'pablo',
  'apellidoPaterno':'juarez',
  'apellidoMaterno':'jimenez',
  'grupos':[],
  'materias':'sin asignar'
  
});


Grupos.agregarGrupo({
  
  'id':12126,
  'clave':'20DPR3047Z',
  'nombre':'Herramientas',
  'alumnos':'sin asignar',
  'docentes':[],
  'materias':'sin asignar'
  
});

Grupos.agregarGrupo({
  
  'id':12156,
  'clave':'cerocero',
  'nombre':'Programacion',
  'alumnos':'sin asignar',
  'docentes':{},
  'materias':'sin asignar'
  
});

Grupos.agregarGrupo({
  
  'id':12157,
  'clave':'20dip',
  'nombre':'BaseDatos',
  'alumnos':'sin asignar',
  'docentes':{},
  'materias':'sin asignar'
  
});

Grupos.agregarGrupo({
  
  'id':12158,
  'clave':'qwerty',
  'nombre':'Taller',
  'alumnos':'sin asignar',
  'docentes':{},
  'materias':'sin asignar'
  
});


Grupos.agregarGrupo({
  
  'id':12159,
  'clave':'cgtre',
  'nombre':'Matematicas',
  'alumnos':'sin asignar',
  'docentes':{},
  'materias':'sin asignar'
  
});

Grupos.agregarGrupo({
  
  'id':12160,
  'clave':'czxcvz',
  'nombre':'Español',
  'alumnos':'sin asignar',
  'docentes':{},
  'materias':'sin asignar'
  
});



Grupos.agregarDocente({
  'id':12156,
  'docente':12
});

Grupos.agregarDocente({
  'id':12160,
  'docente':12
});

Grupos.agregarDocente({
  'id':12156,
  'docente':12
});


Grupos.listarGrupos();
Docentes.listarDocentes();


 
